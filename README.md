# StudyCards #

Enkel eksamen quiz med relevant spørsmål og svar til respektive fag.
Spørmsål og svar er ekstrahert fra eldre eksamenssett på INFOVIT ved Det samfunnsvitenskapelige fakultet, UiB. 


### Kontakt ###

All kode er skrevet av Arein Shibani.
Kontakt kan oppnåes via mail: aaari94@gmail.com